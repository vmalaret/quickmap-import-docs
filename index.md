---
layout: default
---

# QuickMap GeoJSON & CSV Import

QuickMap now supports the dragging and dropping of GeoJSON and CSV files into the application.

When using either GeoJSON or CSV:
- All coordinates are expressed in longitude and latitude degrees.
- All height values are in meters above reference ellipsoid.
- QuickMap attempts to use any feature attribute name containing the following keywords as a feature label (in order of preference): `title`, `label`, `name`, `site`
- The following feature attributes can be used for styling: `stroke`, `fill`, `stroke-width`, `marker-size`, `marker-color`. More info below under styling.

### GeoJSON ###
 
[GeoJSON spec](https://tools.ietf.org/html/rfc7946)

Valid GeoJSON is easily generated from other data formats using ogr:
```
ogr2ogr -f GeoJSON -mapFieldType Date=Integer out.geojson in.shp
```
The mapFieldType options helps handle conversion cases since JSON has no native Date format.


Note: The spec says all coordinates are lon,lat on the WGS84 datum but QuickMap interprets them as lon,lat on whatever currently loaded ellipsoid.


### CSV ###
For CSV, QuickMap support two formats of defining features:

1) The first is a CSV with `lon`,`lat` and an optional `height` column that can be used to define a set of points.

2) A CSV with a `WKT` or `geometry` column that contains the feature geometry encoded as as [WKT](https://en.wikipedia.org/wiki/Well-known_text_representation_of_geometry) string

Internally to QuickMap the CSV is immediately converted to a GeoJSON representation and all other columns are assigned to the feature's `feature.properties` object.

A CSV with a `WKT` column is easily generated from ogr using
```
ogr2ogr -f out.csv in.shp -lco GEOMETRY=AS_WKT
```

### Styling ###

As of now we support only very basic per-feature styling.

Color values are expected to be valid CSS color values of types: `#fff` `#ffffff`, `rgb(255, 255, 255)`, `rgba(255,255,255,1)`

 using the following attributes:

- `stroke` - **Color** the color of the stroke in line or polygon geometries
- `stroke-width` - **Integer** the width of the stroke in pixels
- `fill` - **Color** the color of the fill in a polygon
- `marker-color` - **Color** the color of a point
- `marker-size` - **Integer** the size of a point in pixels


### Example files ###


#### Simple Demo [geojson](https://files.actgate.com/quickmap-import-examples/demo.geojson) [csv](https://files.actgate.com/quickmap-import-examples/demo.csv)


![demo.geojson](https://files.actgate.com/quickmap-import-examples/demo.png)

a simple demo with a few shapes with various style options applied

#### Apollo 17 [geojson](https://files.actgate.com/quickmap-import-examples/Apollo17.geojson) [csv](https://files.actgate.com/quickmap-import-examples/Apollo17.csv)


![Apollo17.geojson](https://files.actgate.com/quickmap-import-examples/Apollo17.png)

using public data shows the tracks of the Apollo 17 luanr rover along with a few key points of interest

#### Copernican Craters [geojson](https://files.actgate.com/quickmap-import-examples/copernican.geojson) [csv](https://files.actgate.com/quickmap-import-examples/copernican.csv)

![copernican.geojson](https://files.actgate.com/quickmap-import-examples/copernican.png)

